# School project
There are some punt of the assignment that are not working, this is because the solution is a personal preference. An no choice has been made at that time.  
This project has been created with Visual Studio 2012 on Windows 7.  

If this project is run in debug mode, it will work because everything is present.

**Note**:  
While checking this code some time ago, it did not work on a newer machine. Due to lack of time i did not search for the cause.  

# database
In this project the embedded version of [FirebirdSQL](http://www.firebirdsql.org/)
is used, to edit the database [flamerobin](http://flamerobin.org/) is used.  
In this embed version of FirebirdSQL stored procedure are used.

# Installation
There is no installation.  
Build in the correct mode and copy the next files to the target location
```
/fb/*.*
/fbembed.dll
/firebird.conf
/ib_util.dll
/icudt30.dll
/icuin30.dll
/icuuc30.dll
/TREE.FDB
/FirebirdSQL.Data.FirebirdClient.dll
/assignment_TreeView.exe
/assignment_TreeView.exe.config
/DatabaseClasses.dll
/Microsoft.Practices.Unity.dll
/WPF.MichaelAgroskin.dll
```
The .config is needed, take all look at it if you want to know why.
