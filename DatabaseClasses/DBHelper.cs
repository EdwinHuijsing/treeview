#region COPYRIGHTS
/*
    - - - - - - - - - - - - - - - - - - - - - - -
    File Name : DBHelper.cs
    - - - - - - - - - - - - - - - - - - - - - - -
    System		:
    Module		:  	Data Layer
    Author		:	Shyam SS
    Date		:	28 June 2007
    Function	:	Define the Data Wrapper Class which provides the functionalities of DBProvider Factory.
    Desctiption	:
 */
#endregion

#region DIRECTIVES

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Configuration;
//using FirebirdSql.Data.FirebirdClient.

#endregion

// This class is converted to static, this is why there can be errors now !!
/*
 * 1. CloseFactoryConnection staat niet op de goede plaats
 * 2. Exception handeling controlen
 * 3. Uitleg zetten bij en in de functies
 * 4. cmdText leeg maken, null zetten, ...? wat te doen. Moet er iets mee gedaan worden?
 * 5. setting uit config laden, Gebruik provider voor keuze connectie string + bestands naam opgeslagen in dien bestaat
 *    In andere class (basis)?
 * 6 Aanmaken DB insert, update, delete tables?
 */
namespace DataBase
{
    /// <summary>
    /// Define the Data Wrapper Class which provides the functionalities of DBProvider Factory.
    /// </summary>
    public static class DBHelper
    {

        #region DECLARATIONS

        private static DbProviderFactory   oFactory;
        private static DbConnection        oConnection;
        private static ConnectionState     oConnectionState;
        public  static DbCommand           oCommand;
        private static DbParameter         oParameter;
        private static DbTransaction       oTransaction;
        private static bool                mblTransaction;

        private static bool m_DebugMode;
        public static bool debugMode
        {
            get { return m_DebugMode; }
            set { m_DebugMode = value; }
        }

        //private static readonly string S_CONNECTION     = ConfigurationManager.AppSettings["DATA.CONNECTIONSTRING"];
        private static string f_ConnectionString;
        public static string a_ConnectionString
        {
            get { return f_ConnectionString; }
            set { f_ConnectionString = value.Trim();
            }
        }

        //private static readonly string S_PROVIDER       = ConfigurationManager.AppSettings["DATA.PROVIDER"];
        private static string f_Provider;
        public static string a_Provider
        {
            get { return f_Provider; }
            set
            {
                f_Provider = value.Trim();
                oFactory = DbProviderFactories.GetFactory(a_Provider);
            }
        }

        #endregion
/* ******************************         ****************************** */
        #region ENUMERATORS

        public enum TransactionType : uint
        {
            Open        = 1,
            Commit      = 2,
            Rollback    = 3
        }

        #endregion
/* ******************************         ****************************** */
        #region STRUCTURES

        /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:
        ///OutPut			:
        ///Comments			:
        /// </summary>
        ///<example>
        /// Remove !-- and --
        /// public IList<!--Entity.Employee--> GetEmployeeDetails(Entity.Department oDepartment)
        ///{
        ///    IList<!--Entity.Employee--> oList    = null;
        ///    Entity.Employee oEmployee       = null;
        ///    DBHelper oDBHelper              = null;
        ///    ArrayList colParameters         = null;
        ///
        ///    try
        ///    {
        ///        colParameters       = new DBHelper.Parameters[]
        ///                                    { new DBHelper.Parameters("@DEPARTMENTID",  oDepartment.Id) }; // Structure Parameter Array
        ///        oDBHelper           = new DBHelper();
        ///        using ( IDataReader drEmployee = oDBHelper.ExecuteReader( CommandType.StoredProcedure, "USP_EMPLOYEE_SELECT", colParameters ) )
        ///        {
        ///            oList   = new List<!--Entity.Employee-->();
        ///            //Reading the reader one by one and setting into requestTypeInfo.
        ///            while (drEmployee.Read())
        ///            {
        ///                oEmployee   = new Entity.Employee();
        ///                oEmployee.Id            = drEmployee["EMPLOYEEID"].ToString();
        ///                oEmployee.Name          = drEmployee["EMPLOYEENAME"].ToString();
        ///                oEmployee.Designation   = drEmployee["DESIGNATION"].ToString();
        ///                oList.Add(oEmployee);
        ///                oEmployee   = null;
        ///            }
        ///        }
        ///        return oList;
        ///    }
        ///    catch (Exception ex)
        ///    {
        ///        throw ex;
        ///    }
        ///    finally
        ///    {
        ///        colParameters   = null;
        ///        oDBHelper       = null;
        ///    }
        ///}
        ///</example>
        public struct Parameters
        {
            public string ParamName;
            public object ParamValue;
            public ParameterDirection ParamDirection;

            public Parameters(string Name, object Value)
            {
                ParamName       = Name;
                ParamValue      = Value;
                ParamDirection  = ParameterDirection.Input;
            }

            public Parameters(string Name, object Value, ParameterDirection Direction)
            {
                ParamName       = Name;
                ParamValue      = Value;
                ParamDirection  = Direction;
            }

        }

        #endregion

/* ******************************         ****************************** */
        #region CONSTRUCTOR

        //public DBHelper()
        //{
        //    //oFactory = DbProviderFactories.GetFactory(S_PROVIDER);
        //    oFactory = DbProviderFactories.GetFactory(a_Provider);
        //}

        #endregion
/* ******************************         ****************************** */
        #region DESTRUCTOR

        //~DBHelper()
        //{
        //    oFactory = null;
        //}

        #endregion
/* ******************************         ****************************** */
        #region CONNECTIONS

        /// <summary>
        ///Description	    :	This function is used to Open Database Connection
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	NA
        ///OutPut			:	NA
        ///Comments			:
        /// </summary>
        public static void EstablishFactoryConnection()
        {
            /*
            // This check is not required as it will throw "Invalid Provider Exception" on the contructor itself.
            if (0 == DbProviderFactories.GetFactoryClasses().Select("InvariantName='" + S_PROVIDER + "'").Length)
                throw new Exception("Invalid Provider");
            */
            oConnection = oFactory.CreateConnection();

            if (oConnection.State == ConnectionState.Closed)
            {
                //oConnection.ConnectionString    = S_CONNECTION;
                oConnection.ConnectionString    = f_ConnectionString;
                oConnection.Open();
                oConnectionState                = ConnectionState.Open;
            }
        }

        /// <summary>
        ///Description	    :	This function is used to Close Database Connection
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	NA
        ///OutPut			:	NA
        ///Comments			:
        /// </summary>
        public static void CloseFactoryConnection()
        {
            //check for an open connection
            try
            {
                if (oConnection.State == ConnectionState.Open)
                {
                    oConnection.Close();
                    oConnectionState = ConnectionState.Closed;
                }
            }
            catch (DbException oDbErr)
            {
                //catch any SQL server data provider generated error messag
                throw new Exception(oDbErr.Message);
            }
            catch (System.NullReferenceException oNullErr)
            {
                throw new Exception(oNullErr.Message);
            }
            finally
            {
                if (null != oConnection)
                {
                    oConnection.Dispose();
                }
            }
        }

        #endregion
/* ******************************         ****************************** */
        #region TRANSACTION

        /// <summary>
        ///Description	    :	This function is used to Handle Transaction Events
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Transaction Event Type
        ///OutPut			:	NA
        ///Comments			:
        /// </summary>
        public static void TransactionHandler(TransactionType veTransactionType)
        {
            switch (veTransactionType)
            {
                case TransactionType.Open:  //open a transaction
                    try
                    {
                        EstablishFactoryConnection();
                        oTransaction    = oConnection.BeginTransaction();
                        mblTransaction  = true;
                    }
                    catch (InvalidOperationException oErr)
                    {
                        throw new Exception("@TransactionHandler - " + oErr.Message);
                    }
                    break;

                case TransactionType.Commit:  //commit the transaction
                    if (null != oTransaction.Connection)
                    {
                        try
                        {
                            oTransaction.Commit();
                            mblTransaction = false;
                        }
                        catch (InvalidOperationException oErr)
                        {
                            throw new Exception("@TransactionHandler - " + oErr.Message);
                        }
                    }
                    break;

                case TransactionType.Rollback:  //rollback the transaction
                    try
                    {
                        if (mblTransaction)
                        {
                            oTransaction.Rollback();
                        }
                        mblTransaction = false;
                    }
                    catch (InvalidOperationException oErr)
                    {
                        throw new Exception("@TransactionHandler - " + oErr.Message);
                    }
                    break;
            }

        }

        #endregion
/* ******************************         ****************************** */
        #region COMMANDS

        #region PARAMETERLESS METHODS

        /// <summary>
        ///Description	    :	This function is used to Prepare Command For Execution
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Transaction, Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	NA
        ///Comments			:	Has to be changed/removed if object based array concept is removed.
        /// </summary>
        private static void PrepareCommand(bool blTransaction, CommandType cmdType, string cmdText)
        {
            if (oConnection.State != ConnectionState.Open)
            {
                //oConnection.ConnectionString    = S_CONNECTION;
                oConnection.ConnectionString    = f_ConnectionString;
                oConnection.Open();
                oConnectionState                = ConnectionState.Open;
            }

            if (null == oCommand)
                oCommand = oFactory.CreateCommand();

            oCommand.Connection     = oConnection;
            oCommand.CommandText    = cmdText;
            oCommand.CommandType    = cmdType;

            if (blTransaction)
                oCommand.Transaction = oTransaction;
        }

        #endregion

        #region OBJECT BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to Prepare Command For Execution
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Transaction, Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	NA
        ///Comments			:
        /// </summary>
        private static void PrepareCommand(bool blTransaction, CommandType cmdType, string cmdText, object[,] cmdParms)
        {

            if (oConnection.State != ConnectionState.Open)
            {
                //oConnection.ConnectionString    = S_CONNECTION;
                oConnection.ConnectionString    = f_ConnectionString;
                oConnection.Open();
                oConnectionState                = ConnectionState.Open;
            }

            if (null == oCommand)
                oCommand            = oFactory.CreateCommand();

            oCommand.Connection     = oConnection;
            oCommand.CommandText    = cmdText;
            oCommand.CommandType    = cmdType;

            if (blTransaction)
                oCommand.Transaction = oTransaction;

            if (null != cmdParms)
                CreateDBParameters(cmdParms);
        }

        #endregion

        #region STRUCTURE BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to Prepare Command For Execution
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Transaction, Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	NA
        ///Comments			:
        /// </summary>
        private static void PrepareCommand(bool blTransaction, CommandType cmdType, string cmdText, Parameters[] cmdParms)
        {
            // Todo:
            if (oConnection.State != ConnectionState.Open)
            {
                //oConnection.ConnectionString    = S_CONNECTION;
                oConnection.ConnectionString    = f_ConnectionString;
                oConnection.Open();
                oConnectionState                = ConnectionState.Open;
            }

            oCommand                = oFactory.CreateCommand();
            oCommand.Connection     = oConnection;
            oCommand.CommandText    = cmdText;
            oCommand.CommandType    = cmdType;

            if (blTransaction)
                oCommand.Transaction = oTransaction;

            if (null != cmdParms)
                CreateDBParameters(cmdParms);
        }

        private static void PrepareCommandNamed(bool blTransaction, CommandType cmdType, string cmdText, DbParameter[] cmdParms)
        {
            // Todo:
            if (oConnection.State != ConnectionState.Open)
            {
                //oConnection.ConnectionString    = S_CONNECTION;
                oConnection.ConnectionString    = f_ConnectionString;
                oConnection.Open();
                oConnectionState                = ConnectionState.Open;
            }

            oCommand                = oFactory.CreateCommand();
            oCommand.Connection     = oConnection;
            oCommand.CommandText    = cmdText;
            oCommand.CommandType    = cmdType;

            if (blTransaction)
                oCommand.Transaction = oTransaction;

            if (null != cmdParms)
            {
                for (int i = 0; i < cmdParms.Length; i++)
                {
                    oCommand.Parameters.Add(cmdParms[i]);
                }
            }
                //CreateDBParameters(cmdParms);
        }

        #endregion

        #endregion
/* ******************************         ****************************** */
        #region PARAMETER METHODS

        #region OBJECT BASED

        /// <summary>
        ///Description	    :	This function is used to Create Parameters for the Command For Execution
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	2-Dimensional Parameter Array
        ///OutPut			:	NA
        ///Comments			:
        /// </summary>
        private static void CreateDBParameters(object[,] colParameters)
        {
            int i;
            for (i = 0; i < colParameters.Length / 2; i++)
            {
                oParameter                  = oCommand.CreateParameter();
                oParameter.ParameterName    = colParameters[i, 0].ToString();
                oParameter.Value            = colParameters[i, 1];

                oCommand.Parameters.Add(oParameter);
            }
        }

        #endregion

        /// <summary>
        /// Create a Parameter
        /// </summary>
        /// <param name="name">Name off the parameter (string)</param>
        /// <param name="value">Value of the paraeter (object)</param>
        /// <param name="direction">ParameterDirection to use, default is Input</param>
        /// <returns>DbParameter</returns>
        public static DbParameter CreateDBParameters(string name, object value, ParameterDirection direction)
        {
            oParameter               = oFactory.CreateParameter();
            oParameter.ParameterName = name;
            oParameter.Value         = value;
            oParameter.Direction     = direction;

            return oParameter;

        }

        #region STRUCTURE BASED

        /// <summary>
        ///Description	    :	This function is used to Create Parameters for the Command For Execution
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	2-Dimensional Parameter Array
        ///OutPut			:	NA
        ///Comments			:
        /// </summary>
        //private void CreateDBParameters(DbParameter[] colParameters)
        private static void CreateDBParameters(Parameters[] colParameters)
        {
            // Todo: Word dit nog gebruikt? Indien ja moet het blijven bestaand?
            for (int i = 0; i < colParameters.Length; i++)
            {
                Parameters oParam           = (Parameters)colParameters[i];

                oParameter                  = oCommand.CreateParameter();
                oParameter.ParameterName    = oParam.ParamName;
                oParameter.Value            = oParam.ParamValue;
                //oParameter.DbType           = oParam.ParamType;
                oParameter.Direction        = oParam.ParamDirection;
                oCommand.Parameters.Add(oParameter);

            }
        }

        #endregion

        #endregion
/* ******************************         ****************************** */
        #region EXCEUTE METHODS

        #region PARAMETERLESS METHODS

        /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Count of Records Affected
        ///Comments			:
        ///                     Has to be changed/removed if object based array concept is removed.
        /// </summary>
        public static int ExecuteNonQuery(CommandType cmdType, string cmdText)
        {
            try
            {

                EstablishFactoryConnection();
                PrepareCommand(false, cmdType, cmdText);
                return oCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (null != oCommand)
                    oCommand.Dispose();
                CloseFactoryConnection();
            }
        }

        /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Transaction, Command Type, Command Text, 2-Dimensional Parameter Array, Clear Paramaeters
        ///OutPut			:	Count of Records Affected
        ///Comments			:
        ///                     Has to be changed/removed if object based array concept is removed.
        /// </summary>
        public static int ExecuteNonQuery(bool blTransaction, CommandType cmdType, string cmdText)
        {
            try
            {
                PrepareCommand(blTransaction, cmdType, cmdText);
                int val = oCommand.ExecuteNonQuery();

                return val;
            }
            catch (Exception ex)
            {
                if ( blTransaction ) TransactionHandler(TransactionType.Rollback);
                throw ex;
            }
            finally
            {
                if (null != oCommand)
                    oCommand.Dispose();
            }
        }

        #endregion

        #region OBJECT BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array, Clear Parameters
        ///OutPut			:	Count of Records Affected
        ///Comments			:
        /// </summary>
        public static int ExecuteNonQuery(CommandType cmdType, string cmdText, object[,] cmdParms, bool blDisposeCommand)
        {
            try
            {

                EstablishFactoryConnection();
                PrepareCommand(false, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (blDisposeCommand && null != oCommand)
                    oCommand.Dispose();
                CloseFactoryConnection();
            }
        }

         /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Count of Records Affected
        ///Comments			:	Overloaded method.
        /// </summary>
        public static int ExecuteNonQuery(CommandType cmdType, string cmdText, object[,] cmdParms)
        {
            return ExecuteNonQuery(cmdType, cmdText, cmdParms, true);
        }

        /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Transaction, Command Type, Command Text, 2-Dimensional Parameter Array, Clear Paramaeters
        ///OutPut			:	Count of Records Affected
        ///Comments			:
        /// </summary>
        public static int ExecuteNonQuery(bool blTransaction, CommandType cmdType, string cmdText, object[,] cmdParms, bool blDisposeCommand)
        {
            try
            {

                PrepareCommand(blTransaction, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                if ( blTransaction ) TransactionHandler(TransactionType.Rollback);
                throw ex;
            }
            finally
            {
                if (blDisposeCommand && null != oCommand)
                    oCommand.Dispose();
            }
        }

        /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Transaction, Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Count of Records Affected
        ///Comments			:	Overloaded function.
        /// </summary>
        public static int ExecuteNonQuery(bool blTransaction, CommandType cmdType, string cmdText, object[,] cmdParms)
        {
            return ExecuteNonQuery(blTransaction, cmdType, cmdText, cmdParms, true);
        }

        #endregion

        #region STRUCTURE BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, Parameter Structure Array, Clear Parameters
        ///OutPut			:	Count of Records Affected
        ///Comments			:
        /// </summary>
        public static int ExecuteNonQuery(CommandType cmdType, string cmdText, Parameters[] cmdParms, bool blDisposeCommand)
        {
            // Todo:
            try
            {

                EstablishFactoryConnection();
                PrepareCommand(false, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (blDisposeCommand && null != oCommand)
                    oCommand.Dispose();
                CloseFactoryConnection();
            }
        }

        public static int ExecuteNonQueryNamed(CommandType cmdType, string cmdText, DbParameter[] cmdParms, bool blDisposeCommand)
        {
            // Todo:
            try
            {
                EstablishFactoryConnection();
                PrepareCommandNamed(false, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (null != oCommand)
                {
                    oCommand.Dispose();
                    // Todo: Why is the Dispose not working in static
                    // For now we use the next line, if we remove this we wil get a error on the second run
                    oCommand = null;
                }
                CloseFactoryConnection();
            }
        }

        public static int ExecuteNonQueryNamed(bool blTransaction, CommandType cmdType, string cmdText, DbParameter[] cmdParms, bool blDisposeCommand)
        {
            try
            {
                PrepareCommandNamed(blTransaction, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (blDisposeCommand && null != oCommand)
                {
                    if (null != oCommand)
                    {
                        oCommand.Dispose();
                        // Todo: Why is the Dispose not working in static
                        // For now we use the next line, if we remove this we wil get a error on the second run
                        oCommand = null;
                    }
                }
            }
        }

         /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, Parameter Structure Array
        ///OutPut			:	Count of Records Affected
        ///Comments			:	Overloaded method.
        /// </summary>
        public static int ExecuteNonQuery(CommandType cmdType, string cmdText, Parameters[] cmdParms)
        {
            // Todo:
            return ExecuteNonQuery(cmdType, cmdText, cmdParms, true);
        }

        /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Transaction, Command Type, Command Text, Parameter Structure Array, Clear Parameters
        ///OutPut			:	Count of Records Affected
        ///Comments			:
        /// </summary>
        public static int ExecuteNonQuery(bool blTransaction, CommandType cmdType, string cmdText, Parameters[] cmdParms, bool blDisposeCommand)
        {
            // Todo:
            try
            {

                PrepareCommand(blTransaction, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                if ( blTransaction ) TransactionHandler(TransactionType.Rollback);
                throw ex;
            }
            finally
            {
                if (blDisposeCommand && null != oCommand)
                    oCommand.Dispose();
            }
        }

        /// <summary>
        ///Description	    :	This function is used to Execute the Command
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Transaction, Command Type, Command Text, Parameter Structure Array
        ///OutPut			:	Count of Records Affected
        ///Comments			:
        /// </summary>
        public static int ExecuteNonQuery(bool blTransaction, CommandType cmdType, string cmdText, Parameters[] cmdParms)
        {
            // Todo:
            return ExecuteNonQuery(blTransaction, cmdType, cmdText, cmdParms, true);
        }

        #endregion STRUCTURE BASED PARAMETER ARRAY

        #endregion
/* ******************************         ****************************** */
        #region READER METHODS

        #region PARAMETERLESS METHODS

        /// <summary>
        ///Description	    :	This function is used to fetch data using Data Reader
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Data Reader
        ///Comments			:
        ///                     Has to be changed/removed if object based array concept is removed.
        /// </summary>
        public static DbDataReader ExecuteReader(CommandType cmdType, string cmdText)
        {

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work
            try
            {

                EstablishFactoryConnection();
                PrepareCommand(false, cmdType, cmdText);
                DbDataReader dr = oCommand.ExecuteReader(CommandBehavior.CloseConnection);
                oCommand.Parameters.Clear();
                return dr;

            }
            catch (Exception ex)
            {
                CloseFactoryConnection();
                throw ex;
            }
            finally
            {
                if (null != oCommand)
                    oCommand.Dispose();
            }
        }

        #endregion PARAMETERLESS METHODS

        #region OBJECT BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to fetch data using Data Reader
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Data Reader
        ///Comments			:
        /// </summary>
        public static DbDataReader ExecuteReader(CommandType cmdType, string cmdText, object[,] cmdParms)
        {

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work

            try
            {

                EstablishFactoryConnection();
                PrepareCommand(false, cmdType, cmdText, cmdParms);
                DbDataReader dr = oCommand.ExecuteReader(CommandBehavior.CloseConnection);
                oCommand.Parameters.Clear();
                return dr;

            }
            catch (Exception ex)
            {
                CloseFactoryConnection();
                throw ex;
            }
            finally
            {
                if (null != oCommand)
                    oCommand.Dispose();
            }
        }

        #endregion OBJECT BASED PARAMETER ARRAY

        #region STRUCTURE BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to fetch data using Data Reader
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, Parameter AStructure Array
        ///OutPut			:	Data Reader
        ///Comments			:
        /// </summary>
        public static DbDataReader ExecuteReader(CommandType cmdType, string cmdText, Parameters[] cmdParms)
        {
            // Todo:

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work
            try
            {

                EstablishFactoryConnection();
                PrepareCommand(false, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception ex)
            {
                CloseFactoryConnection();
                throw ex;
            }
            finally
            {
                if (null != oCommand)
                    oCommand.Dispose();
            }
        }

        #endregion STRUCTURE BASED PARAMETER ARRAY

        #endregion READER METHODS
/* ******************************         ****************************** */
        #region ADAPTER METHODS

        #region PARAMETERLESS METHODS

        /// <summary>
        ///Description	    :	This function is used to fetch data using Data Adapter
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Data Set
        ///Comments			:
        ///                     Has to be changed/removed if object based array concept is removed.
        /// </summary>
        public static DataSet DataAdapterDataSet(CommandType cmdType, string cmdText)
        {

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work

            DbDataAdapter dda   = null;
            try
            {
                EstablishFactoryConnection();
                dda = oFactory.CreateDataAdapter();
                PrepareCommand(false, cmdType, cmdText);

                dda.SelectCommand   = oCommand;
                DataSet ds          = new DataSet();
                dda.Fill(ds);
                return ds;
            }
            catch (Exception /*ex*/)
            {
                //throw ex;
                return null;
            }
            finally
            {
                if (null != oCommand)
                {
                    oCommand.Dispose();
                    // Todo: Why is the Dispose not working in static
                    // For now we use the next line, if we remove this we wil get a error on the second run
                    oCommand = null;
                }
                cmdText = null;
                CloseFactoryConnection();
            }
        }

        /// <summary>
        ///Description	    :	This function is used to fetch data using Data Adapter
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Data Table
        ///Comments			:
        ///                     Has to be changed/removed if object based array concept is removed.
        /// </summary>
        public static DataTable DataAdapterDataTable(CommandType cmdType, string cmdText)
        {

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work

            DbDataAdapter dda   = null;
            try
            {
                EstablishFactoryConnection();
                dda = oFactory.CreateDataAdapter();
                PrepareCommand(false, cmdType, cmdText);

                dda.SelectCommand   = oCommand;
                DataTable dt          = new DataTable();
                dda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                if ( debugMode ) throw ex; // Some where a oCommand is not set to NULL, in moste cases.
                return null;
            }
            finally
            {
                if (null != oCommand)
                {
                    oCommand.Dispose();
                    // Todo: Why is the Dispose not working in static
                    // For now we use the next line, if we remove this we wil get a error on the second run
                    oCommand = null;
                }

                cmdText = null;
                CloseFactoryConnection();
            }
        }

        #endregion PARAMETERLESS METHODS

        #region OBJECT BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to fetch data using Data Adapter
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Data Set
        ///Comments			:
        /// </summary>
        public static DataSet DataAdapterDataSet(CommandType cmdType, string cmdText, object[,] cmdParms)
        {

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work
            DbDataAdapter dda = null;
            try
            {
                EstablishFactoryConnection();
                dda = oFactory.CreateDataAdapter();
                PrepareCommand(false, cmdType, cmdText, cmdParms);

                dda.SelectCommand   = oCommand;
                DataSet ds          = new DataSet();
                dda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                //throw ex;
                return null;
            }
            finally
            {
                if (null != oCommand)
                {
                    oCommand.Dispose();
                    // Todo: Why is the Dispose not working in static
                    // For now we use the next line, if we remove this we wil get a error on the second run
                    oCommand = null;
                }
                cmdText = null;
                CloseFactoryConnection();
            }
        }

        public static DataTable DataAdapterDataTable(CommandType cmdType, string cmdText, object[,] cmdParms)
        {

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work
            DbDataAdapter dda = null;
            try
            {
                EstablishFactoryConnection();
                dda = oFactory.CreateDataAdapter();
                PrepareCommand(false, cmdType, cmdText, cmdParms);

                //dda.SelectCommand   = oCommand;
                dda.InsertCommand = oCommand;
                DataTable dt          = new DataTable();
                //dda.Fill(dt);
                dda.Update(dt);

                CloseFactoryConnection();

                return dt;
            }
            catch (Exception /*ex*/)
            {
                //throw ex;
                CloseFactoryConnection();
                return null;
            }
            finally
            {
                if (null != oCommand)
                {
                    oCommand.Dispose();
                    // Todo: Why is the Dispose not working in static
                    // For now we use the next line, if we remove this we wil get a error on the second run
                    oCommand = null;
                }
                cmdText = null;
                //CloseFactoryConnection();
            }
        }

        #endregion OBJECT BASED PARAMETER ARRAY

        #region STRUCTURE BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to fetch data using Data Adapter
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Data Set
        ///Comments			:
        /// </summary>
        public static DataSet DataAdapter(CommandType cmdType, string cmdText, Parameters[] cmdParms)
        {
            // Todo

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work
            DbDataAdapter dda   = null;
            try
            {
                EstablishFactoryConnection();
                dda = oFactory.CreateDataAdapter();
                PrepareCommand(false, cmdType, cmdText, cmdParms);

                dda.SelectCommand   = oCommand;
                DataSet ds          = new DataSet();
                dda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if(null != oCommand)
                    oCommand.Dispose();
                CloseFactoryConnection();
            }
        }
        public static DataTable DataAdapterDataTable(CommandType cmdType, string cmdText, Parameters[] cmdParms)
        {
            // Todo

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work
            DbDataAdapter dda   = null;
            try
            {
                EstablishFactoryConnection();
                dda = oFactory.CreateDataAdapter();
                PrepareCommand(false, cmdType, cmdText, cmdParms);

                dda.SelectCommand   = oCommand;
                DataTable dt        = new DataTable();
                dda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                if (debugMode) throw ex;
                return null;
            }
            finally
            {
                if(null != oCommand)
                {
                    oCommand.Dispose();
                    // Todo: Why is the Dispose not working in static
                    // For now we use the next line, if we remove this we wil get a error on the second run
                    oCommand = null;
                }
                CloseFactoryConnection();
            }
        }
        public static DataTable DataAdapterDataTable(CommandType cmdType, string cmdText, DbParameter[] cmdParms)
        {
            // Todo

            // we use a try/catch here because if the method throws an exception we want to
            // close the connection throw code, because no datareader will exist, hence the
            // commandBehaviour.CloseConnection will not work
            DbDataAdapter dda   = null;
            try
            {
                EstablishFactoryConnection();
                dda = oFactory.CreateDataAdapter();
                PrepareCommandNamed(false, cmdType, cmdText, cmdParms);

                dda.SelectCommand   = oCommand;
                DataTable dt        = new DataTable();
                dda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                if (debugMode) throw ex;
                return null;
            }
            finally
            {
                if(null != oCommand)
                {
                    oCommand.Dispose();
                    // Todo: Why is the Dispose not working in static
                    // For now we use the next line, if we remove this we wil get a error on the second run
                    oCommand = null;
                }
                CloseFactoryConnection();
            }
        }


        #endregion STRUCTURE BASED PARAMETER ARRAY

        #endregion ADAPTER METHODS
/* ******************************         ****************************** */
        #region SCALAR METHODS

        #region PARAMETERLESS METHODS

        /// <summary>
        ///Description	    :	This function is used to invoke Execute Scalar Method
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Object
        ///Comments			:
        /// </summary>
        public static object ExecuteScalar(CommandType cmdType, string cmdText)
        {
            try
            {
                EstablishFactoryConnection();

                PrepareCommand(false, cmdType, cmdText);

                object val = oCommand.ExecuteScalar();

                return val;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (null != oCommand)
                    oCommand.Dispose();
                CloseFactoryConnection();
            }
        }

        #endregion PARAMETERLESS METHODS

        #region OBJECT BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to invoke Execute Scalar Method
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Object
        ///Comments			:
        /// </summary>
        public static object ExecuteScalar(CommandType cmdType, string cmdText, object[,] cmdParms, bool blDisposeCommand)
        {
            try
            {

                EstablishFactoryConnection();
                PrepareCommand(false, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteScalar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (blDisposeCommand && null != oCommand)
                    oCommand.Dispose();
                CloseFactoryConnection();
            }
        }

         /// <summary>
        ///Description	    :	This function is used to invoke Execute Scalar Method
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Object
        ///Comments			:	Overloaded Method.
        /// </summary>
        public static object ExecuteScalar(CommandType cmdType, string cmdText, object[,] cmdParms)
        {
            return ExecuteScalar(cmdType, cmdText, cmdParms, true);
        }

        /// <summary>
        ///Description	    :	This function is used to invoke Execute Scalar Method
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Object
        ///Comments			:
        /// </summary>
        public static object ExecuteScalar(bool blTransaction, CommandType cmdType, string cmdText, object[,] cmdParms, bool blDisposeCommand)
        {
            try
            {

                PrepareCommand(blTransaction, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteScalar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (blDisposeCommand && null != oCommand)
                    oCommand.Dispose();
            }
        }

        /// <summary>
        ///Description	    :	This function is used to invoke Execute Scalar Method
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Object
        ///Comments			:
        /// </summary>
        public static object ExecuteScalar(bool blTransaction, CommandType cmdType, string cmdText, object[,] cmdParms)
        {
            return ExecuteScalar(blTransaction, cmdType, cmdText, cmdParms, true);
        }

        #endregion OBJECT BASED PARAMETER ARRAY

        #region STRUCTURE BASED PARAMETER ARRAY

        /// <summary>
        ///Description	    :	This function is used to invoke Execute Scalar Method
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Object
        ///Comments			:
        /// </summary>
        public static object ExecuteScalar(CommandType cmdType, string cmdText, Parameters[] cmdParms, bool blDisposeCommand)
        {
            // Todo:
            try
            {
                EstablishFactoryConnection();
                PrepareCommand(false, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteScalar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (blDisposeCommand && null != oCommand)
                    oCommand.Dispose();
                CloseFactoryConnection();
            }
        }

        /// <summary>
        ///Description	    :	This function is used to invoke Execute Scalar Method
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Object
        ///Comments			:	Overloaded Method.
        /// </summary>
        public static object ExecuteScalar(CommandType cmdType, string cmdText, Parameters[] cmdParms)
        {
            // Todo:
            return ExecuteScalar(cmdType, cmdText, cmdParms, true);
        }

        /// <summary>
        ///Description	    :	This function is used to invoke Execute Scalar Method
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Object
        ///Comments			:
        /// </summary>
        public static object ExecuteScalar(bool blTransaction, CommandType cmdType, string cmdText, Parameters[] cmdParms, bool blDisposeCommand)
        {
            // Todo:
            try
            {

                PrepareCommand(blTransaction, cmdType, cmdText, cmdParms);
                return oCommand.ExecuteScalar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (blDisposeCommand && null != oCommand)
                    oCommand.Dispose();
            }
        }

        /// <summary>
        ///Description	    :	This function is used to invoke Execute Scalar Method
        ///Author			:	Shyam SS
        ///Date				:	28 June 2007
        ///Input			:	Command Type, Command Text, 2-Dimensional Parameter Array
        ///OutPut			:	Object
        ///Comments			:
        /// </summary>
        public static object ExecuteScalar(bool blTransaction, CommandType cmdType, string cmdText, Parameters[] cmdParms)
        {
            // Todo:
            return ExecuteScalar(blTransaction, cmdType, cmdText, cmdParms, true);
        }

        #endregion STRUCTURE BASED PARAMETER ARRAY

        #endregion SCALAR METHODS

    }
}


