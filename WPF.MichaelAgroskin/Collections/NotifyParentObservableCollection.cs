﻿// Copyright (C) Michael Agroskin 2010
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace WPF.MichaelAgroskin.Collections
{
    /// <summary>
    /// ObservableCollection attaches to PropertyChanged events of its children
    /// and raises the ChildPropertyChanged event accordingly.
    /// </summary>
    /// <remarks>
    /// Kudos to somebody on the Internet for the idea.
    /// Lost the link. If you wrote it, please tell.
    /// </remarks>
    public class NotifyParentObservableCollection<T> : ObservableCollection<T>
    {
        public NotifyParentObservableCollection() : this(true) {}

        public NotifyParentObservableCollection(bool supportsChangeNotifications)
        {
            SupportsChangeNotifications = supportsChangeNotifications;
        }

        public NotifyParentObservableCollection(IEnumerable<T> collection, bool supportsChangeNotifications = true)
            : base(collection)
        {
            SupportsChangeNotifications = supportsChangeNotifications;
        }

        public NotifyParentObservableCollection(List<T> list, bool supportsChangeNotifications = true)
            : base(list)
        {
            SupportsChangeNotifications = supportsChangeNotifications;
        }

        private bool _supportsChangeNotifications;

        public bool SupportsChangeNotifications
        {
            get { return _supportsChangeNotifications; }
            set
            {
                _supportsChangeNotifications = value;
                if (_supportsChangeNotifications)
                {
                    AttachHandlers(Items);
                }
                else
                {
                    DetachHandlers(Items);
                }
            }
        }

        public event PropertyChangedEventHandler ChildPropertyChanged;

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (SupportsChangeNotifications)
            {
                if (e.Action == NotifyCollectionChangedAction.Add)
                {
                    AttachHandlers(e.NewItems);
                }
                else if (e.Action == NotifyCollectionChangedAction.Remove)
                {
                    DetachHandlers(e.OldItems);
                }
            }

            base.OnCollectionChanged(e);
        }

        protected override void ClearItems()
        {
            if (SupportsChangeNotifications)
            {
                DetachHandlers(Items);
            }

            base.ClearItems();
        }

        private void AttachHandlers(IEnumerable items)
        {
            if (items != null)
            {
                foreach (var item in items.OfType<INotifyPropertyChanged>())
                {
                    item.PropertyChanged += OnChildPropertyChanged;
                }
            }
        }

        private void DetachHandlers(IEnumerable items)
        {
            if (items != null)
            {
                foreach (var item in items.OfType<INotifyPropertyChanged>())
                {
                    item.PropertyChanged -= OnChildPropertyChanged;
                }
            }
        }

        void OnChildPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (!SupportsChangeNotifications)
                return;

            if (ChildPropertyChanged != null)
            {
                ChildPropertyChanged(sender, args);
            }
        }
    }
}
