﻿using System.ComponentModel;

namespace WPF.MichaelAgroskin.Helpers
{
    /// <summary>
    /// Utility class useful for various lookup lists and selectors
    /// </summary>
    public class LookupItem : INotifyPropertyChanged
    {
        private int _index;
        public int Index
        {
            get { return _index; }
            set { _index = value; OnPropertyChanged("Index"); }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; OnPropertyChanged("IsSelected"); }
        }

        private string _caption;
        public string Caption
        {
            get { return _caption; }
            set { _caption = value; OnPropertyChanged("Caption"); }
        }

        private object _value;
        public object Value
        {
            get { return _value; }
            set { _value = value; OnPropertyChanged("Value"); }
        }

        private string _type;
        public string Type
        {
            get { return _type; }
            set { _type = value; OnPropertyChanged("Type"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}


