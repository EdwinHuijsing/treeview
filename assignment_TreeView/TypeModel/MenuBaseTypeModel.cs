﻿namespace assignment_TreeView.TypeModel
{
    using WPF.MichaelAgroskin.Collections;

    /// <summary>
    /// Base class for all ViewModel classes displayed by TreeViewItems.
    /// This acts as an adapter between a raw data object and a TreeViewItem.
    /// Thanks to Josh Smith (Code Proect).
    /// </summary>
    public class MenuBaseTypeModel : FrameWork.BaseViewModel
    {
        // We use const to decalre the name of the properties, this way we use
        // one name on all places
        public const string ISUSERVISIBLEPROPERTYNAME = "IsUserVisible";

        /// <summary>
        /// Here we store the children that belong to this menu entry
        /// </summary>
        private readonly NotifyParentObservableCollection<MenuBaseTypeModel> _children;
        /// <summary>
        /// If this is not a top menu we need to store the parent.
        /// </summary>
        private readonly MenuBaseTypeModel _parent;
        /// <summary>
        /// Here we store the model that belongs to this menu entry
        /// </summary>
        private Model.MenuItemsRighthtModel _Item;

        /// <summary>
        /// Here we store if this menu entry visible to the user.
        /// </summary>
        private bool _IsUserVisible = false;


        #region Constructors
        /// <summary>
        /// Create a new menu entry
        /// </summary>
        /// <param name="item">Model with all the information</param>
        /// <param name="parent">Parent of this menu entry</param>
        protected MenuBaseTypeModel(Model.MenuItemsRighthtModel item, MenuBaseTypeModel parent)
        {
            _Item = item;
            MenuCaption = item.MenuCaption;
            _parent = parent;
            _IsUserVisible = item.IsUserVisible;
            _children = new NotifyParentObservableCollection<MenuBaseTypeModel>();
            _children.ChildPropertyChanged += Children_PropertyChanged;
        }
        #endregion // Constructors

        /// <summary>
        /// Get the model of this menu entry.
        /// </summary>
        public Model.MenuItemsRighthtModel Item
        {
            get { return _Item; }
        }

        /// <summary>
        /// Returns the logical child items of this object.
        /// </summary>
        public NotifyParentObservableCollection<MenuBaseTypeModel> Children
        {
            get { return _children; }
        }

        /// <summary>
        /// Set or get if the menu entry is visible to the user.
        /// </summary>
        public bool IsUserVisible
        {
            get { return _IsUserVisible; }
            set
            {
                if (_IsUserVisible == value) return;

                if (value == false)
                {
                    Item.Create = false;
                    Item.Update = false;
                    Item.Del = false;
                    Item.Search = false;
                    Item.Print = false;
                    Item.TblLookUp = false;
                }
                _IsUserVisible = value;

                this.OnPropertyChanged(ISUSERVISIBLEPROPERTYNAME);
                setIsUserVisible(value);
            }
        }

        /// <summary>
        /// Changes the IsUserVisible property for the childeren and the parent
        /// of this menu entry
        /// </summary>
        /// <param name="value"></param>
        private void setIsUserVisible(bool value)
        {
            if (value)
            {
                if (_parent == null) return;
                _parent.IsUserVisible = true;
            }
            else
            {
                foreach (MenuBaseTypeModel item in Children)
                {
                    if (item.IsUserVisible) item.IsUserVisible = false;
                }
                if (_parent != null) _parent.checkChildren();
            }

        }

        /// <summary>
        /// Check if the Children are visible, wen there are not visible it sets
        /// the visiblitie if this menu entry to false
        /// </summary>
        public void checkChildren()
        {
            bool IsChildVisable = false;
            int cnt = 0;

            while ((cnt < Children.Count) && (IsChildVisable == false))
            {
                IsChildVisable = Children[cnt].IsUserVisible;
                cnt++;
            }

            // Update the visiblity of this menu entry.
            // If one or more children are visible this menu entry wil be visible
            // else this will  not visible.
            this.IsUserVisible = IsChildVisable;
        }

        /// <summary>
        /// Set or get the text of the menu entry
        /// </summary>
        public string MenuCaption
        {
            get { return _Item.MenuCaption; }
            set { _Item.MenuCaption = value.Trim(); }
        }

        //#region command
        ////public FrameWork.DelegateCommand<object> Command { get; private set; }

        ////Command = new FrameWork.DelegateCommand<object>(Command_Execute, Command_CanExecute, "");

        ////private void Command_Execute(object arg)
        ////{
        ////}
        ////private bool Command_CanExecute(object arg)
        ////{ return false; }
        //#endregion command

        /// <summary>
        /// Set this menu and the children back to de default value
        /// </summary>
        public void reset()
        {
            foreach (MenuBaseTypeModel item in Children)
            {
                item.reset();
            }
            if (this.Item.IsMenuGroup) return;
            this.IsUserVisible = false;
            this.Item.resetRights();
            this.IsUserVisible = this.Item.IsUserVisible;
            this.Item.HasChanged = Model.eWhatTodo.Save;
            OnPropertyChanged(string.Empty);
        }

        /// <summary>
        /// Strore rights to the database
        /// </summary>
        public void save()
        {
            if (Children.Count > 0)
            {
                foreach (MenuBaseTypeModel item in Children)
                {
                    item.save();
                }
            }

            _Item.save();

        }

        /// <summary>
        /// Set the rights for this menu entry or this children
        /// </summary>
        /// <param name="dicRights">Colection of menu ids and righ string</param>
        public void setRights(System.Collections.Generic.Dictionary<int, string> dicRights)
        {
            //// If there are no more rigths to set, then were done here.
            //if (dicRights.Count == 0)
            //    return;

            // Set the childrens rights
            for (int i = 0; i < Children.Count; i++)
            {
                Children[i].setRights(dicRights);
            }

            // If this menu entry is in the list set its rights
            if (!this.Item.IsMenuGroup)
            {
                if (dicRights.ContainsKey(this.Item.MenuItemID))
                {
                    // Set the rights for the current Item
                    this.Item.setRights(dicRights[this.Item.MenuItemID]);
                    // Remove the use dictionary entry
                    dicRights.Remove(this.Item.MenuItemID);
                    // Item has not changed, update the proprerty
                    this.Item.HasChanged = Model.eWhatTodo.None;
                    // Check if the items should be visable to the user
                    IsUserVisible = Item.IsUserVisible;

                    // Broadcast all properties
                    OnPropertyChanged(string.Empty);
                }
                else
                {
                    this.Item.resetRights();
                    IsUserVisible = this.Item.IsUserVisible;
                    OnPropertyChanged(string.Empty);
                }
            }
        }

        /// <summary>
        /// Rethrow the PropertyChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Children_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e);
        }

        /// <summary>
        /// Checks if this menu entry is visibe to the user
        /// </summary>
        /// <returns>
        /// Returns visabible if this menu entry is visible to the user, or Collapsed it the menu
        /// entry is not visible to the user
        /// </returns>
        public System.Windows.Visibility UserVisible
        {
            get
            {
                if (IsUserVisible)
                    return System.Windows.Visibility.Visible;
                else
                    return System.Windows.Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Update the user visiblity.
        /// </summary>
        protected void UpdateIsUserVisible()
        {
            if (Item.Create == true || Item.Update == true || Item.Del == true ||
                    Item.Search == true || Item.Print == true || Item.TblLookUp == true)
                IsUserVisible = true;
            else
                IsUserVisible = false;
        }

    } //TreeViewItemViewModel

} // ns assignment_TreeView.ViewModel