﻿namespace assignment_TreeView.TypeModel
{
    /// <summary>
    /// This class is needed to be able customise the treeview last item.
    /// </summary>
    public class MenuItemTypeModel : MenuBaseTypeModel
    {
        #region Construction
        /// <summary>
        /// Setup the items we need, inclouding the base class.
        /// </summary>
        /// <param name="mim"></param>
        public MenuItemTypeModel(Model.MenuItemsRighthtModel mim)
                : this(mim, null){}
        /// <summary>
        /// Setup the items we need, inclouding the base class.
        /// </summary>
        /// <param name="mim"></param>
        /// <param name="parent">Note that we cast the items to the base class.</param>
        public MenuItemTypeModel(Model.MenuItemsRighthtModel mim, MenuBaseTypeModel parent)
                :base(mim, parent)
        {
            if (Item != null)
                Item.PropertyChanged += Item_PropertyChanged;
            RNewCommand = new FrameWork.DelegateCommand<bool?>(RNewCommand_Execute, RNewCommand_CanExecute, "");
            RUpdateCommand = new FrameWork.DelegateCommand<bool?>(RUpdateCommand_Execute, RUpdateCommand_CanExecute, "");
            RDelCommand = new FrameWork.DelegateCommand<bool?>(RDelCommand_Execute, RDelCommand_CanExecute, "");
            RSearchCommand = new FrameWork.DelegateCommand<bool?>(RSearchCommand_Execute, RSearchCommand_CanExecute, "");
            RPrintCommand = new FrameWork.DelegateCommand<bool?>(RPrintCommand_Execute, RPrintCommand_CanExecute, "");
            RTblLookupCommand = new FrameWork.DelegateCommand<bool?>(RTblLookupCommand_Execute, RTblLookupCommand_CanExecute, "");
        }

        #endregion Construction

        #region command
        public FrameWork.DelegateCommand<bool?> RNewCommand { get; private set; }
        public FrameWork.DelegateCommand<bool?> RUpdateCommand { get; private set; }
        public FrameWork.DelegateCommand<bool?> RDelCommand { get; private set; }
        public FrameWork.DelegateCommand<bool?> RSearchCommand { get; private set; }
        public FrameWork.DelegateCommand<bool?> RPrintCommand { get; private set; }
        public FrameWork.DelegateCommand<bool?> RTblLookupCommand { get; private set; }

        private bool RNewCommand_CanExecute(bool? arg)
        { return true; }
        private void RNewCommand_Execute(bool? arg)
        {
            if (Item.Create == arg)
                return;
            Item.Create = arg;
            updateStatus((bool)arg);
        }

        private bool RUpdateCommand_CanExecute(bool? arg)
        { return true; }
        private void RUpdateCommand_Execute(bool? arg)
        {
            if (Item.Update == arg)
                return;
            Item.Update = arg;
            updateStatus((bool)arg);
        }

        private bool RDelCommand_CanExecute(bool? arg)
        { return true; }
        private void RDelCommand_Execute(bool? arg)
        {
            if (Item.Del == arg)
                return;
            Item.Del = arg;
            updateStatus((bool)arg);
        }

        private bool RSearchCommand_CanExecute(bool? arg)
        { return true; }
        private void RSearchCommand_Execute(bool? arg)
        {
            if (Item.Search == arg)
                return;
            Item.Search = arg;
            updateStatus((bool)arg);
        }

        private bool RPrintCommand_CanExecute(bool? arg)
        { return true; }
        private void RPrintCommand_Execute(bool? arg)
        {
            if (Item.Print == arg)
                return;
            Item.Print = arg;
            updateStatus((bool)arg);
        }

        private bool RTblLookupCommand_CanExecute(bool? arg)
        { return true; }
        private void RTblLookupCommand_Execute(bool? arg)
        {
            if (Item.TblLookUp == arg)
                return;
            Item.TblLookUp = arg;
            updateStatus((bool)arg);
        }

        #endregion command

        public bool? Create
        {
            get { return Item.Create; }
            set
            {
                if (Item.Create == value)
                    return;

                if (Item != null)
                    Item.PropertyChanged -= Item_PropertyChanged;

                Item.Create = value;

                if (Item != null)
                    Item.PropertyChanged += Item_PropertyChanged;

                OnPropertyChanged("Create");
                updateStatus((bool)value);
            }
        }
        public bool? Del
        {
            get { return Item.Del; }
            set
            {
                if (Item.Del == value)
                    return;
                Item.Del = value;
                OnPropertyChanged("Del");
                updateStatus((bool)value);
            }
        }
        public bool? Print
        {
            get { return Item.Print; }
            set
            {
                if (Item.Print == value)
                    return;
                Item.Print = value;
                OnPropertyChanged("Print");
                updateStatus((bool)value);
            }
        }
        public bool? Search
        {
            get { return Item.Search; }
            set
            {
                if (Item.Search == value)
                    return;
                Item.Search = value;
                OnPropertyChanged("Search");
                updateStatus((bool)value);
            }
        }
        public bool? TblLookUp
        {
            get { return Item.TblLookUp; }
            set
            {
                if (Item.TblLookUp == value)
                    return;
                Item.TblLookUp = value;
                OnPropertyChanged("TblLookUp");
                updateStatus((bool)value);
            }
        }
        public bool? Update
        {
            get { return Item.Update; }
            set
            {
                if (Item.Update == value)
                    return;
                Item.Update = value;
                OnPropertyChanged("Update");
                updateStatus((bool)value);
            }
        }

        /// <summary>
        /// Update the user visiblity
        /// </summary>
        /// <param name="arg"></param>
        private void updateStatus(bool arg)
        {
            UpdateIsUserVisible();
        }

        /// <summary>
        /// Rethrow the PropertyChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged(e);
        }


    }
}
