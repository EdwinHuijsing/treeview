﻿namespace assignment_TreeView.TypeModel
{
    /// <summary>
    /// This class is needed to be able customise the treeview group items.
    /// </summary>
    public class MenuGroupMainTypeModel : MenuBaseTypeModel
    {
        /// <summary>
        /// Constructor top level menu entry
        /// </summary>
        /// <param name="im">Menu enry model.</param>
        public MenuGroupMainTypeModel(Model.MenuItemsRighthtModel im) : this(im, null) { }

        /// <summary>
        /// Constructor for all levels
        /// </summary>
        /// <param name="im">Menu entry model</param>
        /// <param name="parent">Parent TypeModel</param>
        private MenuGroupMainTypeModel(Model.MenuItemsRighthtModel im, MenuGroupMainTypeModel parent)
            : base(im, parent)
        {
            try
            {
                /// We need to create the correct instance for the TreeViewItem.
                /// This needs to be done in order to customise the TreeView.
                foreach (Model.MenuItemsRighthtModel item in Item.Children)
                {
                    if (item.IsMenuGroup)
                    {
                        /// This is not the last item of the TreeView/Menu.
                        base.Children.Add(new MenuGroupTypeModel(item, this));
                    }
                    else
                    {
                        /// This is the last item of the TreeViewMenu
                        base.Children.Add(new MenuItemTypeModel(item, this));
                    }
                }
            }
            catch (System.Exception ex)
            {
                string t = ex.Message;
            }

        }

    }
}
