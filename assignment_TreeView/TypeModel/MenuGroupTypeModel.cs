﻿namespace assignment_TreeView.TypeModel
{
    /// <summary>
    /// This class is needed to be able customise the treeview group items.
    /// </summary>
    public class MenuGroupTypeModel : MenuBaseTypeModel
    {
        /// <summary>
        /// Contructor for top level menu entrys (with children)
        /// </summary>
        /// <param name="im"></param>
        /// <param name="parent"></param>
        public MenuGroupTypeModel(Model.MenuItemsRighthtModel im, MenuGroupMainTypeModel parent)
            : base(im, parent)
        {
            createTypeModel(im, parent);
        }

        /// <summary>
        /// Contructor for sub level menu entrys (with children
        /// </summary>
        /// <param name="im"></param>
        /// <param name="parent"></param>
        public MenuGroupTypeModel(Model.MenuItemsRighthtModel im, MenuGroupTypeModel parent)
            : base(im, parent)
        {
            createTypeModel(im, parent);
        }

        /// <summary>
        /// Sinds both constructor need to do the same, real constructor work
        /// work is put here.
        /// </summary>
        /// <param name="im"></param>
        /// <param name="parent"></param>
        private void createTypeModel(Model.MenuItemsRighthtModel im, MenuBaseTypeModel parent)
        {
            try
            {
                /// We need to create the correct instance for the TreeViewItem.
                /// This needs to be done in order to customise the TreeView.
                foreach (Model.MenuItemsRighthtModel item in Item.Children)
                {
                    if (item.IsMenuGroup)
                    {
                        /// This is not the last item of the TreeView/Menu.
                        base.Children.Add(new MenuGroupTypeModel(item, this));
                    }
                    else
                    {
                        /// This is the last item of the TreeViewMenu
                        base.Children.Add(new MenuItemTypeModel(item, this));
                    }
                }
            }
            catch (System.Exception ex)
            {
                string t = ex.Message;
            }
        }

    }
}
