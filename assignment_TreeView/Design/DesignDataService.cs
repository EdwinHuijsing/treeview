﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using assignment_TreeView.ViewModel;
using assignment_TreeView.Data;
using System.Collections.ObjectModel;

namespace assignment_TreeView.Design
{
    public class DesignDataService : IDataService
    {
        System.Data.DataTable IDataService.getItems()
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add(new System.Data.DataColumn("MENUNAME"));
            dt.Columns.Add(new System.Data.DataColumn("MENUPARENT"));
            dt.Columns.Add(new System.Data.DataColumn("MENUGROUP"));

            const string NAME = "menuItem";
            string cel2;
            char cel3;
            for (int i = 0; i < 4; i++)
            {
                cel2 = string.Empty;
                for (int ii = 0; ii < 4; ii++)
                {
                    if (ii > 0) cel2 = NAME + i + "-" + (ii - 1);
                    if (ii < 3) cel3 = Convert.ToChar("T");
                            else cel3 = Convert.ToChar("F");

                    dt.Rows.Add(NAME + i + "-" + ii, cel2, cel3);
                }
            }
            return dt;
        }

        bool IDataService.saveItems(Model.MenuItemsRighthtModel itemModel, Action<bool> callback)
        {
 	        throw new NotImplementedException();
        }



        public List<Model.MenuItemsRighthtModel> getRawMenu()
        {
            List<Model.MenuItemsRighthtModel> menu = new List<Model.MenuItemsRighthtModel>();
            menu.Add(new Model.MenuItemsRighthtModel(1, 0, "Parent1", true,
                        new Model.MenuItemsRighthtModel(2, 1, "Sub1", true,
                            new Model.MenuItemsRighthtModel(3, 2, "Item", false, null)
                        )
                    )
            );

            menu.Add(new Model.MenuItemsRighthtModel(4, 0, "Parent2", true,
                        new Model.MenuItemsRighthtModel(2, 4, "Item", false, null)
                    )
            );


            return menu;
        }


        public Dictionary<int, string> getRights(string userName)
        {
            throw new NotImplementedException();
        }
    }//DesignDataService
} //ns
