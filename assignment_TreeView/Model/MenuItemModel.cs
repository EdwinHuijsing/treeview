﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace assignment_TreeView.Model
{
    /// <summary>
    /// Here are the propertys of the a menuitem defined
    /// </summary>
    public class MenuItemModel : FrameWork.BaseObject
    {
        int _MenuIDPK = 0;
        public int MenuItemID
        {
            get { return _MenuIDPK; }
            set
            {
                if (_MenuIDPK == value) return;
                _MenuIDPK = value;
                OnPropertyChanged("MenuItemID");
            }
        }

        int _MenuIDFK = 0;
        public int MenuParentID
        {
            get { return _MenuIDFK; }
            set
            {
                if (_MenuIDFK == value) return;
                _MenuIDFK = value;
                OnPropertyChanged("MenuParentID");
            }
        }

        string _MenuCaption = string.Empty;
        public string MenuCaption
        {
            get { return _MenuCaption; }
            set
            {
                if (_MenuCaption == value) return;
                _MenuCaption = value;
                OnPropertyChanged("MenuCaption");
            }
        }

        bool _MenuIsGroup = false;
        public bool IsMenuGroup
        {
            get { return _MenuIsGroup; }
            set
            {
                if (_MenuIsGroup == value) return;
                _MenuIsGroup = value;
                OnPropertyChanged("IsMenuGroup");
            }
        }

        public MenuItemModel() {}
        public MenuItemModel(int menuIdPk, int menuIdFk, string caption, bool group, params MenuItemModel[] Children)
        {
            _MenuIDPK = menuIdPk;
            _MenuIDFK = menuIdFk;
            _MenuCaption = caption;
            _MenuIsGroup = group;
            if (Children != null)
            {
                foreach (Model.MenuItemModel item in Children)
                {

                    _children.Add(item);
                }

            }
        }

        private ObservableCollection<MenuItemModel> _children = new ObservableCollection<MenuItemModel>();
        /// <summary>
        /// This is used to get the children while converting the datatable to raw data.
        /// </summary>
        /// <todo>
        /// Is this needed or should the data converting be done a other way?
        /// </todo>
        public ObservableCollection<MenuItemModel> Children
        {
            get { return _children; }
            set
            {
                _children = value;
                OnPropertyChanged("Children");
            }
        }

    }
}
