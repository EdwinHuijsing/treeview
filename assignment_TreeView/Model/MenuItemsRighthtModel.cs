﻿namespace assignment_TreeView.Model
{
    using assignment_TreeView.FrameWork;
    using System.Collections.ObjectModel;

    public enum eWhatTodo
    {
        Save,
        None
    }

    /// <summary>
    /// What sql options do we have
    /// </summary>
    /// <remarks>
    /// The sp for update should handel the update and the new records
    /// </remarks>
    enum eWhatTodoSql
    {
        Update,
        Del
    }

    public class MenuItemsRighthtModel : MenuItemModel
    {
        clsMenu _clsMenu = new clsMenu();

        private const string CREATE = "CREATE";     //6+5=11
        private const string DEL = "DEL";        //3+5=8
        private const string PRINT = "PRINT";      //5+5=10
        private const string UPDATE = "UPDATE";     //4+5=9
        private const string SEARCH = "SEARCH";     //6+5=11
        private const string TBLLOOKUP = "TBLLOOKUP";  //9+5=14

        #region members
        private ObservableCollection<MenuItemsRighthtModel> _children = new ObservableCollection<MenuItemsRighthtModel>();

        public const bool defCREATE = true;
        public const bool defREAD = false;
        public const bool defUPDATE = true;
        public const bool defDEL = false;
        public const bool defSEARCH = true;
        public const bool defPRINT = true;
        public const bool defTBLLOOKUP = false;

        private int _RightIdPk;
        private int _UserIdPKFK;

        private bool? _Create = defCREATE;
        private bool? _Read = defREAD;
        private bool? _Update = defUPDATE;
        private bool? _Del = defDEL;
        private bool? _Search = defSEARCH;
        private bool? _Print = defPRINT;
        private bool? _TblLookUp = defTBLLOOKUP;

        private eWhatTodo _WhatTodo = eWhatTodo.None;
        eWhatTodoSql _WhatTodoSql;
        #endregion members

        #region prop
        public int RightIdPk
        {
            get { return _RightIdPk; }
            set
            {
                _RightIdPk = value;
                OnPropertyChanged("RightIdPk");
            }
        }
        public int UserIdPkFk
        {
          get {return _UserIdPKFK;}
          set
          {
              _UserIdPKFK = value;
              OnPropertyChanged("UserIdPkFk");
          }
        }

        public bool? Create
         {
          get {return _Create;}
          set
          {
              if (_Create == value)
                  return;

              _Create = value;
              _WhatTodo = eWhatTodo.Save;
              OnPropertyChanged("Create");
          }
        }
        public bool? Read
         {
          get {return _Read;}
          set
          {
              if (_Read == value)
                  return;
              _Read = value;
              _WhatTodo = eWhatTodo.Save;
              OnPropertyChanged("Read");
          }
        }
        public bool? Update
         {
          get {return _Update;}
          set
          {
              if (_Update == value)
                  return;

              _Update = value;
              _WhatTodo = eWhatTodo.Save;
              OnPropertyChanged("Update");
          }
        }
        public bool? Del
         {
          get {return _Del;}
          set
          {
              if (_Del == value)
                  return;

              _Del = value;
              _WhatTodo = eWhatTodo.Save;
              OnPropertyChanged("Del");
          }
        }
        public bool? Search
         {
          get {return _Search;}
          set
          {
              if (_Search == value)
                  return;

              _Search = value;
              _WhatTodo = eWhatTodo.Save;
              OnPropertyChanged("Search");
          }
        }
        public bool? Print
         {
          get {return _Print;}
          set
          {
              if (_Print == value)
                  return;

              _Print = value;
              _WhatTodo = eWhatTodo.Save;
              OnPropertyChanged("Print");
          }
        }
        public bool? TblLookUp
         {
          get {return _TblLookUp;}
          set
          {
              if (_TblLookUp == value)
                  return;

              _TblLookUp = value;
              _WhatTodo = eWhatTodo.Save;
              OnPropertyChanged("TblLookUp");
          }
        }

        /// <summary>
        /// This is used to get the children while converting the datatable to raw data.
        /// </summary>
        /// <todo>
        /// Is this needed or should the data converting be done a other way?
        /// </todo>
        public new ObservableCollection<MenuItemsRighthtModel> Children
        {
            get { return _children; }
            set
            {
                _children = value;
                OnPropertyChanged("Children");
            }
        }
        #endregion prop


        #region ctor
        public MenuItemsRighthtModel(){}

        public MenuItemsRighthtModel(int menuIdPk, int menuIdFk, string caption, bool group, params MenuItemsRighthtModel[] children)
            : this(menuIdPk, menuIdFk, caption, group, defCREATE, defREAD, false, defDEL, defUPDATE, defSEARCH, defPRINT, defTBLLOOKUP, children)
        {
        }

        public MenuItemsRighthtModel(int menuIdPk, int menuIdFk, string caption, bool group, bool create, bool read, bool write, bool del, bool update,
        bool search, bool print, bool tblLookup)
            : this(menuIdPk, menuIdFk, caption, group, create, read, write, del, update, search, print, tblLookup, null)
        {
        }
        public MenuItemsRighthtModel(int menuIdPk, int menuIdFk, string caption, bool group, bool create, bool read, bool write, bool del, bool update,
                bool search, bool print, bool tblLookup, params MenuItemsRighthtModel[] children)
            : base(menuIdPk, menuIdFk, caption, group, children)
        {
            _Create = create;
            _Read = read;
            _Update = update;
            _Del = del;
            _Search = search;
            _Print = print;
            _TblLookUp = tblLookup;

            if (children != null)
            {
                foreach (Model.MenuItemsRighthtModel item in children)
                {
                    _children.Add(item);
                }

            }


        }
        #endregion ctor

        public bool save()
        {
            if (_WhatTodo == eWhatTodo.None)
                return true;

            check();

            bool ret = false;
            switch (_WhatTodoSql)
            {
                case eWhatTodoSql.Del:
                    ret = del();
                    break;

                case eWhatTodoSql.Update:
                    ret = update();
                    break;
            }

            if (ret)
                HasChanged = eWhatTodo.None;
            return ret;
        }

        /// <summary>
        /// Check what sql option needs to be execute
        /// </summary>
        private void check()
        {
            if (IsUserVisible)
            {
                if (Create == defCREATE && Read == defREAD && Update == defUPDATE && Del == defDEL
                        && Print == defPRINT && TblLookUp == defTBLLOOKUP)
                {
                    _WhatTodoSql = eWhatTodoSql.Del;
                    return;
                }
            }
            else
            {
                Create = false;
                Read = false;
                Update = false;
                Del = false;
                Print = false;
                TblLookUp = false;
            }

            _WhatTodoSql = eWhatTodoSql.Update;
        }

        private bool del()
        {
            _clsMenu.deleteRights(this);
            return false;
        }

        private bool update()
        {
            _clsMenu.updateRights(this);
            return false;
        }

        public eWhatTodo HasChanged
        {
            get { return _WhatTodo; }
            set
            {
                if (_WhatTodo == value)
                    return;

                _WhatTodo = value;
            }
        }

        public bool IsUserVisible
        {
            get
            {
                return ((bool)_Create || (bool)_Del || (bool)_Print || (bool)_Read || (bool)_Search || (bool)_TblLookUp || (bool)_Update) ;
            }
        }

        /// <summary>
        /// Convert Rights a string that can be stored in the database
        /// </summary>
        /// <param name="model">Model that need to be converted</param>
        /// <returns>Retruns a string that van be stored in the database. Or empty is there is a error</returns>
        public string getRightsString()
        {
            System.Text.StringBuilder ret = new System.Text.StringBuilder();
            try
            {
                getRightsStringAdd(ret, CREATE, Create);
                getRightsStringAdd(ret, DEL, Del);
                getRightsStringAdd(ret, PRINT, Print);
                getRightsStringAdd(ret, SEARCH, Search);
                getRightsStringAdd(ret, TBLLOOKUP, TblLookUp);
                getRightsStringAdd(ret, UPDATE, Update);
                return ret.ToString();
            }
            catch (System.Exception)
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Add a new property to the createbuilder that is used to create
        /// a string to store in the database
        /// </summary>
        /// <param name="sb">StringBuilder to add the propterty to.</param>
        /// <param name="propName">Name of the property.</param>
        /// <param name="value">Value of the property.</param>
        private void getRightsStringAdd(System.Text.StringBuilder sb, string propName, bool? value)
        {
            sb.Append(propName + ":" + value.ToString() + ";");
        }

        /// <summary>
        /// Creaete a rights model and set the rights properties.
        /// </summary>
        /// <param name="rightsString">String to convert.</param>
        public void setRights(string rightsString)
        {
            string[] rights = rightsString.Split(new char[] { ';' });
            string[] rightPair;

            foreach (string right in rights)
            {
                if (string.IsNullOrEmpty(right))
                        continue;

                rightPair = right.Split(new char[] { ':' });
                bool value = System.Convert.ToBoolean(rightPair[1]);
                switch (rightPair[0])
                {
                    case CREATE:
                        Create = value;
                        break;
                    case UPDATE:
                        Update = value;
                        break;
                    case DEL:
                        Del = value;
                        break;
                    case SEARCH:
                        Search = value;
                        break;
                    case PRINT:
                        Print = value;
                        break;
                    case TBLLOOKUP:
                        TblLookUp = value;
                        break;
                }
            }
        }

        public void resetRights()
        {
            this.Create = defCREATE;
            this.Del = defDEL;
            this.Print = defPRINT;
            this.Search = defSEARCH;
            this.TblLookUp = defTBLLOOKUP;
            this.Update = defUPDATE;
        }
    }
}

// tblRigthTypes
// tblUserRights (UserId, RigthType) if right is removed record has to be deleted