﻿using System;
using System.Data;

namespace assignment_TreeView
{
    public enum eExecSql
    {
        Select,
        NoSelect
    }

    public class clsBasis
    {
        private static string m_ErrorMsg = string.Empty;
        public static string a_ErrorMsg { get { return m_ErrorMsg; } }
        protected static string a_ErrorMsgSet { set { m_ErrorMsg = value; } }

        protected const string RETURNVALUE = "ReturnValue";

        #region var
        protected DataTable dt;
        //protected DataRow dr;
        ////protected DataView dv;
        ////protected DataSet ds;
        protected int result = 0;
        protected DataBase.DBHelper.Parameters[] arrParam;
        #endregion var

        /// <summary>
        /// Checks the result from the stored porcedure
        /// </summary>
        /// <param name="result"></param>
        /// <returns>If all is good then returns true, IF there are errors returns false and set a errormessages</returns>
        protected bool checkResult(int result)
        {
            switch (result)
            {
                case 999:
                    a_ErrorMsgSet = string.Empty;
                    return true;
                case 998:
                    a_ErrorMsgSet = "Dubbel ingave is niet toegestaan.";
                    return false;
                case 997:
                    a_ErrorMsgSet = "Er heeft zich een fout voor gedaan tijden het wegschrijven.\nHet wegschrijven is niet gelukt";
                    return false;
                case 996: //update
                    a_ErrorMsgSet = "Er heeft zich een fout voor gedaan tijden het aanpassen.\nHet aanpassen is niet gelukt";
                    return false;
                case 995: //update
                    a_ErrorMsgSet = "Er heeft zich een fout voor gedaan tijden het verwijderen.";
                    return false;
                case 900:
                    //a_ErrorMsgSet = "A error occurred. Message: " + ex;
                    return false;

                // http://msdn.microsoft.com/en-us/library/aa937592%28v=SQL.80%29.aspx
                case 201:
                    a_ErrorMsgSet = "Er ontbreekt minimaal een parameter";
                    return false;
                case 257:
                    a_ErrorMsgSet = "Er word een verkeerd value type aangelevert\n\n denk aan nvarchar in plaats van Timestamp\n dit kan ook het gevolg zijn als de verkeerde stored procedure word gebruikt\nStaat alles in de goede volgorde?";
                    return false;
                case 515:
                    // http://msdn.microsoft.com/en-us/library/aa258724%28v=sql.80%29.aspx
                    a_ErrorMsgSet = "Een veld dat ingevult moet zijn is niet ingevult";
                    return false;
                case 547:
                    // Constraint probleem
                    a_ErrorMsgSet = "Er is een conflict met de gegevens gevonden!";
                    return false;
                default:
                    a_ErrorMsgSet = "Er heeft zich een nog onbekende fout voor gedaan";
                    return false;
            }

        }

//        #region dbhelper
        #region ExecuteNonQuery
        protected bool ExecuteNonQuery(String storedProcedureName)
        {
            dt = null;
            return ExecuteNonQuery(storedProcedureName, null, false);
        }
        protected bool ExecuteNonQuery(String storedProcedureName, System.Data.Common.DbParameter[] arrParam)
        {
            dt = null;
            return ExecuteNonQuery(storedProcedureName, arrParam, false);
        }
        protected bool ExecuteNonQuery(String storedProcedureName, System.Data.Common.DbParameter[] arrParam, bool trans)
        {
            try
            {
                if (trans)
                {
                    DataBase.DBHelper.TransactionHandler(DataBase.DBHelper.TransactionType.Open);
                    DataBase.DBHelper.ExecuteNonQueryNamed(true, CommandType.StoredProcedure, storedProcedureName, arrParam, true);
                }
                else
                {
                    DataBase.DBHelper.ExecuteNonQueryNamed(CommandType.StoredProcedure, storedProcedureName, arrParam, true);
                }
            }
            catch (System.Data.Common.DbException ex)
            {
                // There is a error, so we rollback de transaction
                if (trans) DataBase.DBHelper.TransactionHandler(DataBase.DBHelper.TransactionType.Rollback);
                //if ( debugMode ) throw ex;
                return false;
            }

            for (int i = 0; i < arrParam.Length; i++)
            {
                if (arrParam[i].ParameterName == RETURNVALUE) result = Convert.ToInt32(arrParam[i].Value);
            }

            if (trans)
            {
                if (result == 999)
                    // Al is oke so we commit the transaction
                    DataBase.DBHelper.TransactionHandler(DataBase.DBHelper.TransactionType.Commit);
                else
                    // A error? so somthing went wrong but wath? anyway we havo to rollback the transaction
                    DataBase.DBHelper.TransactionHandler(DataBase.DBHelper.TransactionType.Rollback);
            }

            return checkResult(result);
        }
        #endregion ExecuteNonQuery

        #region ExecuteStoredProcedure
        protected bool ExecuteStoredProcedure(string storedProcedureName)
        {
            dt = null;
            return ExecuteStoredProcedure(storedProcedureName, null, ref dt, eExecSql.Select);
        }
        protected bool ExecuteStoredProcedure(string storedProcedureName, DataBase.DBHelper.Parameters[] arrParam, eExecSql execSql)
        {
            dt = null;
            return ExecuteStoredProcedure(storedProcedureName, arrParam, ref dt, execSql);

        }
        protected bool ExecuteStoredProcedure(string storedProcedureName, DataBase.DBHelper.Parameters[] arrParam, ref System.Data.DataTable dataTable, eExecSql execSql)
        {
            try
            {
                if (arrParam != null)
                {
                    //dataTable = DatabaseClasses.Dac.MsSql.ExecuteStoredProcedure.ExecuteDataTable(
                    //dataTable = DatabaseClasses.Dac.FBsql.ExecuteStoredProcedure.ExecuteDataTable(
                    dataTable = DataBase.DBHelper.DataAdapterDataTable(
                                                CommandType.StoredProcedure
                                                , storedProcedureName
                                                , arrParam
                                                );
                    if (execSql == eExecSql.Select)
                        // with a select stored procedure there is no need to check the result
                        return true;
                    else
                    {
                        for (int i = 0; i < arrParam.Length; i++)
                        {
                            if (arrParam[i].ParamName == RETURNVALUE) result = Convert.ToInt32(arrParam[i].ParamValue);
                        }
                        return checkResult(result);
                    }
                }
                else
                {
                    // This wil allways be a select stored procedure
                    //dataTable = DatabaseClasses.Dac.MsSql.ExecuteStoredProcedure.ExecuteDataTable(
                    //dataTable = DatabaseClasses.Dac.FBsql.ExecuteStoredProcedure.ExecuteDataTable(
                    dataTable = DataBase.DBHelper.DataAdapterDataTable(
                                                CommandType.StoredProcedure
                                                , storedProcedureName
                                                );
                    return true;
                }
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                return checkResult(e.Number);
            }

        }

        protected bool ExecuteComStoredProcedure(string storedProcedureName, System.Data.Common.DbParameter[] arrParam, eExecSql execSql)
        {
            dt = null;
            return ExecuteComStoredProcedure(storedProcedureName, arrParam, ref dt, execSql);

        }
        protected bool ExecuteComStoredProcedure(string storedProcedureName, System.Data.Common.DbParameter[] arrParam, ref System.Data.DataTable dataTable, eExecSql execSql)
        {
            try
            {
                if (arrParam != null)
                {
                    //dataTable = DatabaseClasses.Dac.MsSql.ExecuteStoredProcedure.ExecuteDataTable(
                    //dataTable = DatabaseClasses.Dac.FBsql.ExecuteStoredProcedure.ExecuteDataTable(
                    dataTable = DataBase.DBHelper.DataAdapterDataTable(
                                                CommandType.StoredProcedure
                                                , storedProcedureName
                                                , arrParam
                                                );
                    if (execSql == eExecSql.Select)
                        // with a select stored procedure there is no need to check the result
                        return true;
                    else
                    {
                        for (int i = 0; i < arrParam.Length; i++)
                        {
                            //if ( (arrParam[i].ParamName == RETURNVALUE ) result = Convert.ToInt32( arrParam[i].ParamValue );
                            if (arrParam[i].ParameterName == RETURNVALUE) result = Convert.ToInt32(arrParam[i].Value);
                        }
                        return checkResult(result);
                    }
                }
                else
                {
                    // This wil allways be a select stored procedure
                    //dataTable = DatabaseClasses.Dac.MsSql.ExecuteStoredProcedure.ExecuteDataTable(
                    //dataTable = DatabaseClasses.Dac.FBsql.ExecuteStoredProcedure.ExecuteDataTable(
                    dataTable = DataBase.DBHelper.DataAdapterDataTable(
                                                CommandType.StoredProcedure
                                                , storedProcedureName
                                                );
                    return true;
                }
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                return checkResult(e.Number);
            }

        }
        #endregion ExecuteStoredProcedure
//        #endregion dbhelper

    }
}