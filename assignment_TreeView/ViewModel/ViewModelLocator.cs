﻿// In App.xaml.cs add using
// In App.xaml add ns + locator keyword, the keyword is used in EditMenu.xaml

using Microsoft.Practices.Unity;

namespace assignment_TreeView.ViewModel
{
    public class ViewModelLocator
    {
        // Setup
        public static IUnityContainer Container
        {
            get;
            private set;
        }
        static ViewModelLocator()
        {
            Container = new UnityContainer();

            if (isInDesignTime)
            {
                Container.RegisterType<Data.IDataService, Design.DesignDataService>();
            }
            else
            {
                Container.RegisterType<Data.IDataService, Data.DataService>();
            }

            Container.RegisterType<EditMenuViewModel>(new ContainerControlledLifetimeManager());
        }
        // Check if we are in design or runtime
        private static bool designTime = System.ComponentModel.DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject());
        public static bool isInDesignTime { get { return designTime; } }

        // Access
        public EditMenuViewModel Main
        {
            get
            {
                return Container.Resolve<EditMenuViewModel>();
            }
        }


    }
}
