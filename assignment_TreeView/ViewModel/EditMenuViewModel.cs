﻿namespace assignment_TreeView.ViewModel
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;

    public class EditMenuViewModel : FrameWork.BaseViewModel
    {
        private List<Model.MenuItemsRighthtModel> rawMenu;
        Data.IDataService _service;

        /// <summary>
        /// Collections of menu item (top of screen)
        /// </summary>
        private ObservableCollection<TypeModel.MenuBaseTypeModel> _MenuItems = new ObservableCollection<TypeModel.MenuBaseTypeModel>();
        /// <summary>
        /// Collection of menu items that will be in the treeview and in the menu above the treeview
        /// </summary>
        private ObservableCollection<TypeModel.MenuBaseTypeModel> _TreeItems = new ObservableCollection<TypeModel.MenuBaseTypeModel>();

        /// <summary>
        /// If a property has changed this on is called to update the HasChanged property
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            HasChanged = true;
        }

        /// <summary>
        /// Get or set the menu collection
        /// </summary>
        public ObservableCollection<TypeModel.MenuBaseTypeModel> MenuItems
        {
            get { return _MenuItems; }
            set
            {
                _MenuItems = value;
                OnPropertyChanged("MenuItems");
            }
        }
        /// <summary>
        /// Get or set the TreeView collection
        /// </summary>
        public ObservableCollection<TypeModel.MenuBaseTypeModel> TreeItems
        {
            get { return _TreeItems; }
            set
            {
                _TreeItems = value;
                OnPropertyChanged("TreeItems");
                OnPropertyChanged("MenuItemTest");
            }
        }
        /// <summary>
        /// If a changes has been made in the TreeView this property will change.
        /// </summary>
        public static bool HasChanged { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service">Data(Base) source</param>
        public EditMenuViewModel(Data.IDataService service)
        {
            _service = service;

            rawMenu = service.getRawMenu();
            foreach (Model.MenuItemsRighthtModel item in rawMenu)
            {
                if (item.IsMenuGroup)
                {
                    MenuItems.Add(new TypeModel.MenuGroupMainTypeModel(item));
                }
                else
                {
                    MenuItems.Add(new TypeModel.MenuItemTypeModel(item));
                }
            }


            // While in design time i want so see content in the treeview
            // At runtime this while be done a button click
            if (ViewModelLocator.isInDesignTime)
                TreeItems = MenuItems;



            // Top buttons
            MenuToTreeCommand = new FrameWork.DelegateCommand<object>(MenuToTreeCommand_Execute, MenuToTreeCommand_CanExecute, "Read menu itemse");
            DummyCommand = new FrameWork.DelegateCommand<object>(DummyCommand_Execute, DummyCommand_CanExecute, "Extract data from the TreeView");
            TrvToMenuVisabeCommand = new FrameWork.DelegateCommand<object>(TrvToMenuVisabeCommand_Execute, TrvToMenuVisabeCommand_CanExecute, "Activate my menu through the TRV");
            CopyCommand = new FrameWork.DelegateCommand<object>(CopyCommand_Execute, CopyCommand_CanExecute, "Kopieer de rechten ingesteld in de TRV");
            PastCommand = new FrameWork.DelegateCommand<object>(PastCommand_Execute, PastCommand_CanExecute, "Plak de rechten ingesteld in de TRV");
            UncheckCommand = new FrameWork.DelegateCommand<object>(UncheckCommand_Execute, UncheckCommand_CanExecute, "Uncheck all TreeviewItems");
            CheckCommand = new FrameWork.DelegateCommand<object>(CheckCommand_Execute, CheckCommand_CanExecute, "Check all TreeviewItems");
            CrudUncheckCommand = new FrameWork.DelegateCommand<object>(CrudResetCommand_Execute, CrudResetCommand_CanExecute, "Reset All TreeviewItems and CRUD checkboxen");
            FillRightsCommand = new FrameWork.DelegateCommand<object>(FillRightsCommand_Execute, FillRightsCommand_CanExecute, "Fill TRV from DB");
            WrtieRightsCommand = new FrameWork.DelegateCommand<object>(WrtieRightsCommand_Execute, WrtieRightsCommand_CanExecute, "Write TRV to DB");
            SetRighsToMenuCommand = new FrameWork.DelegateCommand<object>(SetRighsToMenuCommand_Execute, SetRighsToMenuCommand_CanExecute, "Read From database and ajust menu");
            EmptyTreeCommand = new FrameWork.DelegateCommand<object>(EmptyTreeCommand_Execute, EmptyTreeCommand_CanExecute, "Empty Tree");

        }

        #region Commands
        #region CommandsDeclarations
        public FrameWork.DelegateCommand<object> MenuToTreeCommand { get; private set; }
        public FrameWork.DelegateCommand<object> DummyCommand { get; private set; }
        public FrameWork.DelegateCommand<object> TrvToMenuVisabeCommand { get; private set; }
        public FrameWork.DelegateCommand<object> CopyCommand { get; private set; }
        public FrameWork.DelegateCommand<object> PastCommand { get; private set; }
        public FrameWork.DelegateCommand<object> UncheckCommand { get; private set; }
        public FrameWork.DelegateCommand<object> CheckCommand { get; private set; }
        public FrameWork.DelegateCommand<object> CrudUncheckCommand { get; private set; }
        public FrameWork.DelegateCommand<object> FillRightsCommand { get; private set; }
        public FrameWork.DelegateCommand<object> WrtieRightsCommand { get; private set; }
        public FrameWork.DelegateCommand<object> SetRighsToMenuCommand { get; private set; }
        public FrameWork.DelegateCommand<object> EmptyTreeCommand { get; private set; }
        #endregion CommandsDeclarations

        #region Top Buttons
        private bool MenuToTreeCommand_CanExecute(object arg)
        { return TreeItems.Count < 1; }
        private void MenuToTreeCommand_Execute(object arg)
        {
            ObservableCollection<TypeModel.MenuBaseTypeModel> tempMenu = new ObservableCollection<TypeModel.MenuBaseTypeModel>();
            foreach (Model.MenuItemsRighthtModel item in rawMenu)
            {
                if (item.IsMenuGroup)
                {
                    tempMenu.Add(new TypeModel.MenuGroupMainTypeModel(item));
                }
                else
                {
                    tempMenu.Add(new TypeModel.MenuItemTypeModel(item));
                }
            }


            TreeItems = tempMenu;
            // // This will work BUT both menus will change
            //TreeItems = new ObservableCollection<TypeModel.MenuBaseTypeModel>(MenuItems);

            WPF.MichaelAgroskin.Collections.NotifyParentObservableCollection<TypeModel.MenuBaseTypeModel> colection =
                new WPF.MichaelAgroskin.Collections.NotifyParentObservableCollection<TypeModel.MenuBaseTypeModel>(TreeItems);
            if (colection != null)
            {
                //// The weak events should be used, but i do not yet understand how it works
                //WPF.MichaelAgroskin.WeakEvents.ChildPropertyChangedEventManager.AddListener(colection, this);
                colection.ChildPropertyChanged += Item_PropertyChanged;
            }

            // No changes has been made so far.
            HasChanged = false;
        }

        private bool DummyCommand_CanExecute(object arg)
        { return false; }
        private void DummyCommand_Execute(object arg)
        {
        }

        private bool TrvToMenuVisabeCommand_CanExecute(object arg)
        { return false; }
        private void TrvToMenuVisabeCommand_Execute(object arg)
        {
        }

        private bool CopyCommand_CanExecute(object arg)
        { return false; }
        private void CopyCommand_Execute(object arg)
        {
        }

        private bool PastCommand_CanExecute(object arg)
        { return false; }
        private void PastCommand_Execute(object arg)
        {
        }

        private bool UncheckCommand_CanExecute(object arg)
        { return TreeItems.Count > 0; }
        private void UncheckCommand_Execute(object arg)
        {
            // We only need to loop throu the top nodes, the childs are dealt
            // with in the function we call.
            foreach (TypeModel.MenuBaseTypeModel item in TreeItems)
            {
                item.IsUserVisible = false;
            }
        }

        private bool CheckCommand_CanExecute(object arg)
        { return false; }
        private void CheckCommand_Execute(object arg)
        {
            // We only need to loop throu the top nodes, the childs are dealt
            // with in the function we call.
            foreach (TypeModel.MenuBaseTypeModel item in TreeItems)
            {
                item.IsUserVisible = true;
            }
        }

        private bool CrudResetCommand_CanExecute(object arg)
        { return TreeItems.Count > 0; }
        private void CrudResetCommand_Execute(object arg)
        {
            // We only need to loop throu the top nodes, the childs are dealt
            // with in the function we call.
            foreach (TypeModel.MenuBaseTypeModel item in TreeItems)
            {
                item.reset();
            }
        }

        private bool FillRightsCommand_CanExecute(object arg)
        {
            // If there are no items in the collection false is return, other wise true.
            return TreeItems.Count > 0;
        }
        private void FillRightsCommand_Execute(object arg)
        {
            System.Collections.Generic.Dictionary<int, string> rights;
            rights = _service.getRights(string.Empty);

            // We only need to loop throu the top nodes, the childs are dealt
            // with in the function we call.
            foreach (TypeModel.MenuBaseTypeModel entry in TreeItems)
            {
                entry.setRights(rights);
            }
            HasChanged = false;
        }

        private bool WrtieRightsCommand_CanExecute(object arg)
        { return HasChanged; }
        private void WrtieRightsCommand_Execute(object arg)
        {
            // Here i move the loop to a function this way its easy to see what
            // is happening here.
            // Also de code can be move out of this class.
            save();
        }

        private bool SetRighsToMenuCommand_CanExecute(object arg)
        { return false; }
        private void SetRighsToMenuCommand_Execute(object arg)
        {
        }

        private bool EmptyTreeCommand_CanExecute(object arg)
        { return TreeItems.Count > 0; }
        private void EmptyTreeCommand_Execute(object arg)
        {
            TreeItems = new ObservableCollection<TypeModel.MenuBaseTypeModel>();
        }

        #endregion Top Buttons

        #endregion commands

        /// <summary>
        /// Loop throu the TreeView items and save them.
        /// </summary>
        private void save()
        {
            try
            {
                foreach (TypeModel.MenuBaseTypeModel item in TreeItems)
                {
                    item.save();
                }
                HasChanged = false;
            }
            catch (System.Exception ex)
            {
                //Environment.NewLine or (char)13
                MessageBox.Show(ex.Message + (char)13 + ex.StackTrace);
            }
        }

    }
}
