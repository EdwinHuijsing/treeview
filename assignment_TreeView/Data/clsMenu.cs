﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace assignment_TreeView
{
    class clsMenu : clsBasis
    {
        #region prop
        //public string MenuItem { get; set; }
        //public string MenuCaption { get; set; }
        public string MenuName { get; set; }
        //public string MenuText { get; set; }
        public string MenuParent { get; set; }
        //private int m_MenuNiveau = 0;
        //public int MenuLevel { get { return m_MenuNiveau; } set { m_MenuNiveau = value; } }
        //public string a_MenuFunc { get; set; }
        #endregion prop

        #region const
        protected const string MENUITEM = "MenuItemID";
        protected const string MENUNAME = "MenuNaam";
        protected const string MENUTEXT = "MenuText";
        protected const string MENUPARENT = "MenuParentID";
        protected const string MENULEVEL = "MenuNiveau";
        #endregion const

        private const string I_Menu = "MENU_I";
        private const string D_Menu = "";
        private const string U_Menu = "";

        public bool addNode()
        {
            System.Data.Common.DbParameter menuName = DataBase.DBHelper.CreateDBParameters(MENUNAME, MenuName, System.Data.ParameterDirection.Input);
            System.Data.Common.DbParameter parentID = DataBase.DBHelper.CreateDBParameters(MENUPARENT, MenuParent, System.Data.ParameterDirection.Input);

            System.Data.Common.DbParameter returnValue = DataBase.DBHelper.CreateDBParameters(RETURNVALUE, 0, System.Data.ParameterDirection.Output); ;

            // Add named parameters
            System.Data.Common.DbParameter[] arrParam = new System.Data.Common.DbParameter[]
            {
                menuName, parentID
                , returnValue
            };

            bool error = ExecuteNonQuery(I_Menu, arrParam);
            if (error && dt != null)
                return true;
            else
                return false;
        }
        public bool deleteNode(string name)
        {
            //throw new NotImplementedException
            return false;

        }
        public bool updateNode()
        {
            //throw new NotImplementedException
            return false;
        }

        public bool deleteRights(Model.MenuItemsRighthtModel model)
        {
            System.Data.Common.DbParameter menuItemID = DataBase.DBHelper.CreateDBParameters(MENUITEM, model.MenuItemID, System.Data.ParameterDirection.Input);
            System.Data.Common.DbParameter userID = DataBase.DBHelper.CreateDBParameters("USERID", "Tester", System.Data.ParameterDirection.Input);

            System.Data.Common.DbParameter returnValue = DataBase.DBHelper.CreateDBParameters(RETURNVALUE, 0, System.Data.ParameterDirection.Output); ;

            // Add named parameters
            System.Data.Common.DbParameter[] arrParam = new System.Data.Common.DbParameter[]
            {
                menuItemID, userID, returnValue
            };

            bool error = ExecuteNonQuery("D_RIGHTS", arrParam);
            if (error && dt != null)
                return true;
            else
                return false;

        }
        public bool updateRights(Model.MenuItemsRighthtModel model)
        {
            System.Data.Common.DbParameter menuItemID   =
                    DataBase.DBHelper.CreateDBParameters(MENUITEM, model.MenuItemID, System.Data.ParameterDirection.Input);
            System.Data.Common.DbParameter userID =
                    DataBase.DBHelper.CreateDBParameters("USERID", "Tester", System.Data.ParameterDirection.Input);
            System.Data.Common.DbParameter Rights =
                    DataBase.DBHelper.CreateDBParameters("RIGHTS", model.getRightsString(), System.Data.ParameterDirection.Input);

            System.Data.Common.DbParameter returnValue =
                    DataBase.DBHelper.CreateDBParameters(RETURNVALUE, 0, System.Data.ParameterDirection.Output); ;

            // Add named parameters
            System.Data.Common.DbParameter[] arrParam = new System.Data.Common.DbParameter[]
            {
                menuItemID, userID, Rights
                , returnValue
            };

            bool error = ExecuteNonQuery("U_Rights", arrParam, true);
            if (error && dt != null)
                return true;
            else
            return false;
        }

        //private const string CREATE     = "CREATE";     //6+5=11
        //private const string DEL        = "DEL";        //3+5=8
        //private const string PRINT      = "PRINT";      //5+5=10
        //private const string UPDATE     = "UPDATE";     //4+5=9
        //private const string SEARCH     = "SEARCH";     //6+5=11
        //private const string TBLLOOKUP  = "TBLLOOKUP";  //9+5=14
        ///// <summary>
        ///// Convert Rights a string that can be stored in the database
        ///// </summary>
        ///// <param name="model">Model that need to be converted</param>
        ///// <returns>Retruns a string that van be stored in the database. Or empty is there is a error</returns>
        //private string rightsToString(Model.MenuItemsRighthtModel model)
        //{
        //    StringBuilder ret = new StringBuilder();
        //    try
        //    {
        //        rightsToStringAdd(ret, CREATE, model.Create);
        //        rightsToStringAdd(ret, DEL, model.Del);
        //        rightsToStringAdd(ret, PRINT, model.Print);
        //        rightsToStringAdd(ret, SEARCH, model.Search);
        //        rightsToStringAdd(ret, TBLLOOKUP, model.TblLookUp);
        //        rightsToStringAdd(ret, UPDATE, model.Update);
        //        return ret.ToString();
        //    }
        //    catch (Exception)
        //    {
        //        return string.Empty;
        //    }

        //}
        ///// <summary>
        ///// Add a new property to the createbuilder that is used to create
        ///// a string to store in the database
        ///// </summary>
        ///// <param name="sb">StringBuilder to add the propterty to.</param>
        ///// <param name="propName">Name of the property.</param>
        ///// <param name="value">Value of the property.</param>
        //private void rightsToStringAdd(StringBuilder sb, string propName, bool? value)
        //{
        //    sb.Append(propName + ":" + value.ToString() + ";");
        //}

        ///// <summary>
        ///// Creaete a rights model and set the rights properties.
        ///// </summary>
        ///// <param name="rightsString">String to convert.</param>
        //private void stringToRights(string rightsString)
        //{
        //    Model.MenuItemsRighthtModel retModel = new Model.MenuItemsRighthtModel();

        //    string[] rights = rightsString.Split(new char[] { ';' });
        //    string[] rightPair;

        //    foreach (string right in rights)
        //    {
        //        rightPair = right.Split(new char[] { ':' });
        //        bool value = Convert.ToBoolean(rightPair[1]);
        //        switch (rightPair[0])
        //        {
        //            case CREATE:
        //                retModel.Create = value;
        //                break;
        //            case UPDATE:
        //                retModel.Update = value;
        //                break;
        //            case DEL:
        //                retModel.Del = value;
        //                break;
        //            case SEARCH:
        //                retModel.Search = value;
        //                break;
        //            case PRINT:
        //                retModel.Print = value;
        //                break;
        //            case TBLLOOKUP:
        //                retModel.TblLookUp = value;
        //                break;

        //        }
        //    }
        //}

    }

}
//11+8+10+9+11+14
//11+9+10+8+11+14