﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using DataBase;


namespace assignment_TreeView.Data
{
    /// <summary>
    /// Connection to database
    /// </summary>
    public class DataService : clsBasis, IDataService
    {
        /// <summary>
        /// Setup the connectio to the database)
        /// </summary>
        /// <returns>Returns True if setup is completed, False if there is a error</returns>
        /// <remarks>This is not the way to work in production</remarks>
        public static bool setupConnectionInfo()
        {
            try
            {
                if (DBHelper.a_ConnectionString == null)
                    DBHelper.a_ConnectionString = "ServerType=1;User=SYSDBA;Password=masterkey;Database=tree.fdb";

                if (DBHelper.a_Provider == null)
                    DBHelper.a_Provider = "FirebirdSql.Data.FirebirdClient";
                return true;
            }
            catch (System.Exception ex)
            {
                string a = ex.Message;
            }
            return false;
        }

        public System.Data.DataTable getItems()
        {
            try
            {
                if (!setupConnectionInfo()) return null;

                System.Data.DataTable dt;
                dt = DBHelper.DataAdapterDataTable(System.Data.CommandType.Text, "SELECT * FROM TBLMENU Order by MENUIDPK, MenuIDFK, MENUCAPTION");

                return dt;
            }
            catch (System.Exception ex)
            {
                string a = ex.Message;
            }

            return null;
        }

        bool IDataService.saveItems(Model.MenuItemsRighthtModel itemsViewModel, Action<bool> callback)
        {
            throw new NotImplementedException();
        }

        public List<Model.MenuItemsRighthtModel> getRawMenu()
        {
            /// Get the items from ther server, this is done in a BLENDABLE way.
            System.Data.DataTable dt = getItems();

            /// Parent MenuItem
            Model.MenuItemsRighthtModel parent = null;
            /// Child MenuItem
            ObservableCollection<Model.MenuItemsRighthtModel> child = null;
            List<Model.MenuItemsRighthtModel> menu = new List<Model.MenuItemsRighthtModel>();

            /// Loop throug alle items and get the root items.
            /// This should be convertend to Linq
            foreach (System.Data.DataRow row in dt.Rows)
            {
                if ((int)row[1] == 0)
                {
                    // Parent
                    parent = new Model.MenuItemsRighthtModel(
                                            (int)row[0], (int)row[1], (string)row[2],  (string)row[3] == "T");

                    // Child(ren)
                    child = getChildren(dt, (int)row[0]);
                    if (child != null) parent.Children = child;
                    menu.Add(parent);
                }
            }
            return menu;
        }
        /// <summary>
        /// Get all children for this item
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private ObservableCollection<Model.MenuItemsRighthtModel> getChildren(System.Data.DataTable dt, int parentId)
        {
            /// Child MenuItem
            ObservableCollection<Model.MenuItemsRighthtModel> child = new ObservableCollection<Model.MenuItemsRighthtModel>();
            /// Parent MenuItem
            Model.MenuItemsRighthtModel parent = null;
            /// List of parents to return
            ObservableCollection<Model.MenuItemsRighthtModel> list = new ObservableCollection<Model.MenuItemsRighthtModel>();

            foreach (System.Data.DataRow row in dt.Rows)
            {
                if ((int)row[1] == parentId)
                {
                    parent = new Model.MenuItemsRighthtModel(
                        (int)row[0], (int)row[1], (string)row[2], (string)row[3] == "T");
                    list.Add(parent);

                    child = getChildren(dt, (int)row[0]);
                    if (child != null) parent.Children = child;
                }
            }
            return list;
        }

        public System.Collections.Generic.Dictionary<int, string> getRights(string userName)
        {
            System.Collections.Generic.Dictionary<int, string> res = new Dictionary<int, string>();
            Model.MenuItemsRighthtModel rights = new Model.MenuItemsRighthtModel();
            string menuRights = string.Empty;

            arrParam = new DBHelper.Parameters[]
            {
                new DataBase.DBHelper.Parameters("UserName", "Tester", System.Data.ParameterDirection.Input)
            };


            System.Data.DataTable dt = null;
            ExecuteStoredProcedure("S_RIGHTS", arrParam, ref dt, eExecSql.Select);

            foreach (System.Data.DataRow row in dt.Rows)
            {
                res.Add(Convert.ToInt32(row[0]), row[1].ToString());
            }

            return res;
        }

    }
}
