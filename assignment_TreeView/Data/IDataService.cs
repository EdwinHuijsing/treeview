﻿using System;
using System.Collections.Generic;
using assignment_TreeView.ViewModel;
using System.Collections.ObjectModel;

namespace assignment_TreeView.Data
{
    public interface IDataService
    {
        System.Data.DataTable getItems();
        List<Model.MenuItemsRighthtModel> getRawMenu();

        bool saveItems(Model.MenuItemsRighthtModel itemsViewModel, Action<bool> callback);
        System.Collections.Generic.Dictionary<int, string> getRights(string userName);
    }
}
